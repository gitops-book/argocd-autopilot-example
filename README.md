argocd-autopilot-example
===

Repo structure created with argocd-autopilot 0.4.12.

```bash
  export GIT_TOKEN=ghp_PcZ...IP0
export GIT_REPO=https://gitlab.com/gitops-book//argocd-autopilot-example

# Basic setup
argocd-autopilot repo bootstrap

# Add first app
argocd-autopilot project create staging
argocd-autopilot app create my-app -p staging --app github.com/argoproj-labs/argocd-autopilot/examples/demo-app/

# Add stage to app
argocd-autopilot project create production
argocd-autopilot app create my-app -p production --app github.com/argoproj-labs/argocd-autopilot/examples/demo-app/
``` 

See https://argocd-autopilot.readthedocs.io/en/stable/Getting-Started/
